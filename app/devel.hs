{-# LANGUAGE PackageImports #-}
import "lunakhod" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
